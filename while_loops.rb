#loops the code 10 times (starting with 1 runs while 1 is less than 10)
i = 1
while i<=10
	puts i*14
	i=i+1
end

#initially more is 'True' to run while loop for atleast once
more = true 
while more==true

  puts "Enter you name"
  name = gets.chomp
  puts "Maths Marks"
  maths_marks = gets.chomp.to_i
  puts "Science marks"
  science_marks = gets.chomp.to_i
  puts "English marks"
  english_marks = gets.chomp.to_i
  puts "Computer marks"
  computer_marks = gets.chomp.to_i 

  total = maths_marks+science_marks+english_marks+computer_marks

  percentage = (total/400.0)*100

  puts "#{name} your total marks is #{total} and your percentage is #{percentage}"

  #Ask for another iteration 
  puts "Want to enter more y/n"
  a = gets.chomp


  # if a!="y"
  #   #if user enters other key than 'y' then making 'more' => false
  #   more = false
  # end
 
  # other ways to break the loop
  if a == 'n'
    break
  end
  
end

#nested while loops

a = 5
b = 1
while a>0  
  while b<=5
    puts "*"*b
    b = b+1
    a = a-1
  end
end